## Snake\_Assemble
Snake assemble is a snakemake based pipeline for building oxford nanopore based assemblies. Inputs are specified using the config file system in either json or yaml format. This workflow is kind of like a wrapper around: [flye](https://github.com/fenderglass/Flye), [wtdbg2](https://github.com/ruanjue/wtdbg2), [miniasm](https://github.com/lh3/miniasm), [racon](https://github.com/isovic/racon), [pilon](https://github.com/broadinstitute/pilon), and [medaka](https://github.com/nanoporetech/medaka).

## Requirements
1. Conda
2. Snakemake

## Usage
There is one required key and many optional keys that can be included in config files for the snake assemble pipeline. These keys are described below...

1. basecalls : A string representing a path to a fastq.gz file containing ONT reads to be assembled. (REQUIRED)
2. basename : A string that will be used to name all output files. Optional. Defaults to inherit from basecalls
4. tools : A list of strings representing the steps that you want taken. Permitted string paterns are: miniasm, wtdbg2, flye, racon{>=1}, pilon{>=1}, medaka{>=1}. Of those, miniasm, wtdbg2, and flye are available assemblers and should come first in the list. The last three, medaka, racon and pilon, are polishers which can be executed iteratively any specified number of times represented by the number following the tool name. For example "racon3" means execute racon three times on the assembly output by the previous tool. Optional. Deftaults to ["miniasm", "racon3"]
5. targs : A list of filenames specifying pipeline products to make. If targets is specified, tools will be ignored.
5. short\_reads : A list of strings representing paths to fastq(.gz) files containing short potentially paired end reads. This is a required input if pilon polishing is requested and is ignored otherwise
6. genome\_size : A string representing an estimate of the assemblies expected size. Example: "135m", "3.2g", etc. This is a required input if wtdbg2 or flye assembly is requested and is ignored otherwise
7. medaka\_opts : A string passed as part of the medaka command. Very useful for specifying which medaka model to use for example. 

...To execute the pipeline, invoke snakemake using something like the following...

```
snakemake --use-conda -j 32 -p \
    --snakefile /path/to/snake_assemble/Snakefile \
    --configfile /path/to/config.json \
    --directory /path/to/outdir/ \
```

Consult the [official snakemake docs](https://snakemake.readthedocs.io/en/stable/executing/cli.html) for more information on the many options and ways that snakemake can be exectued.

This repo includes an example config that can be used to test your installation. You will need to grab the fastq files for SRR11460449 from SRA, and probably edit the config file to point to your reads. Note that the reads need to be gzipped and that all paths in your config files will be interpretted by snakemake relative to your output directory.
