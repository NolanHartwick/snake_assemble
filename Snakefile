import os



def trim_str(s, subs):
    """ Trim substrings from the end of s until none of subs are present

        >>> trim_str("a.fasta.gz", [".gz"])
        "a.fasta"
        >>> trim_str("a.fasta.gz", [".fasta"])
        "a.fasta.gz"
        >>> trim_str("a.fasta.gz.fasta", [".gz", ".fasta"])
        "a"
        >>> trim_str("a.some_other_extension", [".gz", ".fasta"])
        "a.some_other_extension"
    """
    flag = True
    while(flag):
        flag = False
        for sub in subs:
            if(s[-1 * len(sub):] == sub):
                s = s[:-1 * len(sub)]
                flag = True
    return s


basecalls = config["basecalls"]
outbase = config.get(
    "outbase",
    os.path.basename(trim_str(basecalls, [".gz", ".fastq", ".fq"]))
)
tools = config.get(
    "tools",
    ["miniasm", "racon3"]
)
short_reads = config.get(
    "short_reads",
    []
)
genome_size = config.get("genome_size")

profile = config.get("profile", "instance").lower()

opts = config.get("opts", {})
targs = config.get("targs", None)

MAX_THREADS = 32


# check that short reads are provided if pilon is requested
if(any(t.startswith("pilon") for t in tools) and len(short_reads) == 0):
    raise ValueError(
        "pilon was requested but short reads haven't been provided"
    )

if(
    any(t == "flye" or t == "wtdbg2" for t in tools) and
    genome_size is None
):
    raise ValueError(
        "flye or wtdbg2 assembly was requested but genome_size hasn't been provided"
    )


# check that given profile is valid
valid_profiles = {
    "instance": lambda : "all",
}
if(profile not in valid_profiles):
    raise ValueError(
        f"Invalid profile. Choose one of: {valid_profiles}"
    )

group_func = valid_profiles[profile]


if(targs is None):
    targs = outbase + "".join("." + t for t in tools) + ".fasta"


envs = {
    "main": "envs/assemble.yml",
    "medaka": "envs/medaka.yml"
}

rule all:
    input:
        targs
    group:
        group_func()


rule copy_reads:
    input:
        basecalls
    output:
        temp(outbase + ".fastq.gz")
    group:
        group_func()
    shell:
        "cp {input} {output}"


rule copy_short:
    input:
        short_reads
    output:
        [
            temp(outbase + ".R1.fastq.gz"),
            temp(outbase + ".R2.fastq.gz")
        ]
    threads:
        MAX_THREADS
    group:
        group_func()
    shell:
        "cp {input[0]} {output[0]} && cp {input[1]} {output[1]}"


rule overlap:
    input:
        rules.copy_reads.output[0]
    output:
        temp(outbase + ".paf")
    threads:
        MAX_THREADS
    conda:
        envs["main"]
    params:
        opt = opts.get("overlap", "")
    group:
        group_func()
    shell:
        "minimap2 -x ava-ont {params[opt]} -t {threads} {input[0]} {input[0]} > {output[0]} "


rule minimap_short:
    input:
        ref = "{assembly}.fasta",
        reads = rules.copy_short.output[0]
    output:
        temp("{assembly}.sam")
    conda:
        envs["main"]
    threads:
        MAX_THREADS
    params:
        opt = opts.get("minimap_short", "")
    group:
        group_func()
    shell:
        "minimap2 -ax sr {params[opt]} -t {threads} {input[ref]} {input[reads]} > {output}"


rule sam_to_bam:
    input:
        "{basename}.sam"
    output:
        temp("{basename}.sorted.bam")
    conda:
        envs["main"]
    threads:
        16
    group:
        group_func()
    shell:
        "samtools view -u {input[0]} | samtools sort -m 2G -@ {threads} -o {output[0]}"


rule index_bam:
    input:
        "{basename}.sorted.bam"
    output:
        temp("{basename}.sorted.bam.bai")
    conda:
        envs["main"]
    group:
        group_func()
    shell:
        "samtools index {input[0]}"


rule minimap_long:
    input:
        ref = "{assembly}.fasta",
        reads = rules.copy_reads.output[0]
    output:
        temp("{assembly}.paf")
    conda:
        envs["main"]
    threads:
        MAX_THREADS
    group:
        group_func()
    params:
        opt = opts.get("minimap_long", "")
    shell:
        "minimap2 -x map-ont {params[opt]} -t {threads} {input[ref]} {input[reads]} > {output}"


rule miniasm:
    input:
        overlaps = rules.overlap.output[0],
        reads = rules.copy_reads.output[0]
    output:
        outbase + ".miniasm.gfa"
    conda:
        envs["main"]
    params:
        opt = opts.get("miniasm", "")
    group:
        group_func()
    shell:
        "miniasm {params[opt]} -f {input[1]} {input[0]} > {output[0]}"


rule gfa_to_fa:
    input:
        ["{assembly}.gfa"]
    output:
        ["{assembly}.fasta"]
    group:
        group_func()
    shell:
        'awk \'/^S/{{print ">"$2"\\n"$3}}\' {input[0]} | fold > {output[0]}'


rule flye:
    input:
        rules.copy_reads.output[0]
    output:
        outbase + ".flye.fasta"
    params:
        outdir = outbase + ".flye",
        opt = opts.get("flye", "")
    conda:
        envs["main"]
    threads:
        MAX_THREADS
    group:
        group_func()
    shell:
        """
        flye {params[opt]} --nano-raw {input[0]} --genome-size {genome_size} --threads {threads} --out-dir {params[outdir]}
        cp {params[outdir]}/assembly.fasta {output[0]}
        # rm -rf {params[outdir]}
        """


rule wtdbg2:
    input:
        rules.copy_reads.output[0]
    output:
        outbase + ".wtdbg2.fasta"
    params:
        outpre = outbase + ".wtdbg2",
        opt = opts.get("wtdbg2", "")
    conda:
        envs["main"]
    threads:
        MAX_THREADS
    group:
        group_func()
    shell:
        """
        mkdir {params[outpre]}
        wtdbg2 {params[opt]} -i {input[0]} -o {params[outpre]}/assembly -t {threads} -x ont -g {genome_size}
        wtpoa-cns -t {threads} -i {params[outpre]}/assembly.ctg.lay.gz -fo {params[outpre]}/assembly.ctg.slf.fa
        ln -s {params[outpre]}/assembly.ctg.slf.fa {output[0]}
        """


def decriment_input(toolname, reads=True, extensions=[]):
    """ Helper function that generates input file names for some polishing
        tool.
    """
    def ret_fun(wcs):
        assem = wcs["assembly"]
        n = int(wcs["n"])
        ret = {}
        if(reads):
            ret["reads"] = rules.copy_reads.output[0]
        for ext in extensions:
            if(n == 1):
                ret[ext] = f"{assem}.{ext}"
            else:
                ret[ext] = f"{assem}.{toolname}{n - 1}.{ext}"
        return ret

    return ret_fun


rule racon:
    input:
        unpack(decriment_input("racon", extensions=["paf", "fasta"]))
    output:
        ["{assembly}.racon{n}.fasta"]
    conda:
        envs["main"]
    threads:
        MAX_THREADS
    params:
        opt = opts.get("racon", "")
    group:
        group_func()
    shell:
        "racon {params[opt]} -t {threads} {input[reads]} {input[paf]} {input[fasta]} > {output[0]}"


rule medaka:
    input:
        unpack(decriment_input("medaka", extensions=["fasta"]))
    output:
        ["{assembly}.medaka{n}.fasta"]
    params:
        wk_dir = lambda wcs: f"{wcs['assembly']}.medaka{wcs['n']}.wkdir",
        opt = opts.get("medaka", "")
    threads:
        MAX_THREADS
    conda:
        envs["medaka"]
    group:
        group_func()
    shell:
        """
        medaka_consensus \
        -t {threads} {params[opt]} \
        -i {input[reads]} -d {input[fasta]} -o {params[wk_dir]}
        mv {params[wk_dir]}/consensus.fasta {output[0]}
        rm -rf {params[wk_dir]}
        """


rule pilon:
    input:
        unpack(decriment_input("pilon", reads=False, extensions=["sorted.bam", 'fasta', "sorted.bam.bai"]))
    output:
        ["{assembly}.pilon{n}.fasta"]
    conda:
        envs["main"]
    params:
        outbase = lambda wcs : f"{wcs['assembly']}.pilon{wcs['n']}",
        opt = opts.get("pilon", "-Xmx160G")
    threads:
        MAX_THREADS
    group:
        group_func()
    shell:
        "pilon {params[opt]} --genome {input[fasta]} --bam {input[sorted.bam]} --output {params.outbase} --threads {threads} --changes"
